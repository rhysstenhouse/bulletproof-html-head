A blank HTML document which has all the metadata structure you need to get to work on building modern websites.

Includes:
* a pre-structured set of favicon and icon tags that should work no matter what device you’re viewing the site on (see: [Understand the favicon](http://www.jonathantneal.com/blog/understand-the-favicon/) and [everything you always wanted to know about touch icons](http://mathiabynens.be/notes/touch-icons))
* a sample of [Open Graph Protocol](http://ogp.me) tags to get you started
* a smattering of basic meta tags to build up from
* some CSS and JS links to show you where they should go in the structure.
